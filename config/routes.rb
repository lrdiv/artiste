Artiste::Application.routes.draw do
  devise_for :users do
    get "logout" => "devise/sessions#destroy"
  end

  root to: "home#index"

  namespace :admin do
    root to: "dashboard#index"

    resources :categories do
      post :sort, on: :collection
    end
    resources :works do
      post :sort, on: :collection
    end
    resources :pages do
      post :sort, on: :collection
    end
    resources :events do
      post :sort, on: :collection
    end
    resources :inquiries do
      post :archive, on: :member
      post :unarchive, on: :member
    end
    resources :users
    resources :settings, only: [:index, :edit, :update]
  end

  resources :works, only: [:index, :show]
  resources :events, only: [:index, :show]
  resources :pages, only: [:show]
  resources :inquiries, only: [:new, :create]

end
