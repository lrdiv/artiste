class CreateWorks < ActiveRecord::Migration
  def change
    create_table :works do |t|
      t.references :category
      t.string :title
      t.string :caption
      t.datetime :produced_at
      t.boolean :public
      t.boolean :featured
      t.string :image
      t.timestamps
    end
  end
end
