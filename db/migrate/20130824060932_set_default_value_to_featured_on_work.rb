class SetDefaultValueToFeaturedOnWork < ActiveRecord::Migration
  def up
    change_column :works, :featured, :boolean, default: false
    change_column :works, :public, :boolean, default: true
  end

  def down
    change_column :works, :featured, :boolean
    change_column :works, :public, :boolean
  end
end
