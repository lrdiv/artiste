class AddFeaturedToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :featured, :boolean
  end
end
