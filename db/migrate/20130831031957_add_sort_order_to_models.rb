class AddSortOrderToModels < ActiveRecord::Migration
  def change
    add_column :categories, :sort_order, :integer
    add_column :events, :sort_order, :integer
    add_column :pages, :sort_order, :integer
    add_column :works, :sort_order, :integer
  end
end
