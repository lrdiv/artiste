class AddArchivedFlagToInquiries < ActiveRecord::Migration
  def change
    add_column :inquiries, :archived, :boolean, default: false
  end
end
