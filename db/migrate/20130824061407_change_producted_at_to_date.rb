class ChangeProductedAtToDate < ActiveRecord::Migration
  def up
    change_column :works, :produced_at, :date
  end

  def down
    change_column :works, :produced_at, :datetime
  end
end
