# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Setting.create(title: "Dismiss Admin Banner", content: "no")
Setting.create(title: "Artist Name", content: "")
Setting.create(title: "Artist Type", content: "")
Setting.create(title: "City", content: "")
Setting.create(title: "State", content: "")
Setting.create(title: "Phone", content: "")

Page.create(title: "About", body: "")
Page.create(title: "Contact", body: "")