# encoding: utf-8

class ImageUploader < CarrierWave::Uploader::Base

  include CarrierWave::RMagick

  storage :fog

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :thumb do
    process resize_to_fill: [100, 100]
  end

  version :small do
    process resize_to_fill: [250, 250]
  end

  version :medium do
    process resize_to_fill: [500, 500]
  end

  version :lightbox do
    process resize_to_fit: [750, 750]
  end

  version :large do
    process resize_to_fit: [1000, 1000]
  end
  

  def extension_white_list
    %w(jpg jpeg gif png)
  end


end
