$(document).ready ->
  $('tbody.sortable-table').sortable
    axis: 'y'
    handle: '.sort-handle'
    helper: (e, ui) ->
      ui.children().each ->
        $(@).width $(@).width()
      return ui
    start: (e, ui) ->
      ui.placeholder.height ui.item.height()
    update: ->
      $.ajax
        type: 'post'
        url: $(@).attr 'data-update-url'
        data: $(@).sortable 'serialize'
        error: ->
          $('.col-tables').prepend('<p class="alert alert-error alert-sortable">Could not sort. Please try again.</p>')
        success: ->
          $('.col-tables').prepend('<p class="alert alert-success alert-sortable">Items sorted successfully!</p>')
        complete: ->
          $('.alert-sortable').delay(2000).fadeOut()