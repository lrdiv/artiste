global =
  closeElement: ->
    $('a.close-element').click ->
      $(@).parent().fadeOut('slow')

  bindWysiwyg: ->
    $('.simple_form textarea').each (i, elem) ->
      $(elem).wysihtml5()

  appendPreloader: ->
    preloader = $('.preloader')
    preloader.hide()
    $('.simple_form').submit (e) ->
      submit = $(@).find('input[type=submit]')
      preloader.show()
      submit.hide()

  bindDateTimePickers: ->
    $('.datetimepicker').datetimepicker
      dateFormat: "yy-mm-dd"
      timeFormat: "HH:mm:ss"
    $('.datepicker').datepicker
      dateFormat: "yy-mm-dd"



$(document).ready ->
  global.closeElement()
  global.bindWysiwyg()
  global.appendPreloader()
  global.bindDateTimePickers()
