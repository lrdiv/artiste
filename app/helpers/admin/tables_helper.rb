module Admin::TablesHelper

  def page_excerpt body=nil, length=150
    unless body.nil?
      safe = strip_tags(body)
      trunc = truncate(safe, length: length)
    end
  end

  def pretty_date datestamp
    unless datestamp.nil?
      date = datestamp.strftime('%B %d, %Y')
    end
  end

  def pretty_date_time timestamp
    unless timestamp.nil?
      datetime = timestamp.in_time_zone('America/Chicago').strftime('%B %d, %Y at %l:%M %p')
    end
  end

  def abbr_date_time timestamp
    unless timestamp.nil?
      datetime = timestamp.in_time_zone('America/Chicago').strftime('%m/%d/%Y %l:%M %p')
    end
  end

end
