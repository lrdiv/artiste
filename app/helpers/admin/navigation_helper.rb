module Admin::NavigationHelper

  def active_for_controller kontroller
    return "active" if controller.controller_name == kontroller
  end

end