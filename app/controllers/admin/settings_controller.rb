class Admin::SettingsController < Admin::AdminController

  def index
    @settings = Setting.all
  end

  def show
    render action: "edit"
  end

  def edit
    @setting = Setting.find(params[:id])
  end

  def update
    @setting = Setting.find(params[:id])
    if @setting.update_attributes(params[:setting])
    else
    end
  end

end
