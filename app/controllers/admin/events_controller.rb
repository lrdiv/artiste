class Admin::EventsController < Admin::AdminController

  before_filter :find_event, only: [:show, :edit, :destroy]

  def index
    @events = Event.sorted.all
    @upcoming_events = Event.sorted.upcoming
    @previous_events = Event.sorted.previous
  end

  def show
    render action: 'edit'
  end

  def new
    @event = Event.new
  end

  def create
    @event = Event.new(params[:event])
    @event.event_at = @event.event_at + 5.hours
    if @event.save
      crud_success "event", "saved"
    else
      crud_failure "event", "new", "saved"
    
    end
  end

  def edit
  end

  def update
    @event = Event.find(params[:id])
    @event.event_at = @event.event_at + 5.hours
    if @event.update_attributes(params[:event])
      crud_success "event", "updated"
    else
      crud_failure "event", "edit", "updated", @event
    end
  end

  def destroy
    if @event.destroy
      # crud_success("deleted")
      crud_success "event", "deleted"
    else
      # crud_failure ("deleted", "index")
      crud_failure "event", "index", "deleted"
    end
  end

  def sort
    params[:event].each_with_index do |id, index|
      Event.update_all({sort_order: index+1}, {id: id})
    end
    render nothing: true
  end

  private

    def find_event
      @event = Event.find(params[:id])
    end

end
