class Admin::UsersController < Admin::AdminController

  before_filter :find_user, only: [:show, :edit, :update, :destroy]

  def index
    @users = User.all
  end

  def show
    render action: 'edit'
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(params[:user])
    if @user.save
      crud_success "user", "saved"
    else
      crud_failure "user", "new", "saved"
    end
  end

  def edit
  end

  def update
    if @user.update_attributes(params[:user])
      crud_success "user", "updated"
    else
      crud_failure "user", "edit", "updated", @user
    end
  end

  def destroy
    if @user.destroy
      crud_success "user", "deleted"
    else
      crud_failure "user", "index", "deleted"
    end
  end

  private

    def find_user
      @user = User.find(params[:id])
    end

end
