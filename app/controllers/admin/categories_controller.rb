class Admin::CategoriesController < Admin::AdminController

  before_filter :find_category, only: [:show, :edit, :update, :destroy]

  def index
    @categories = Category.sorted.all
  end

  def show
    render action: 'edit'
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(params[:category])
    if @category.save
      crud_success "category", "saved"
    else
      crud_failure "category", "new", "saved"
    end
  end

  def edit
  end

  def update
    if @category.update_attributes(params[:category])
      crud_success "category", "updated"
    else
      crud_failure "category", "edit", "updated", @category
    end
  end

  def destroy
    if @category.destroy
      crud_success "category", "deleted"
    else
      crud_failure "category", "index", "deleted"
    end
  end

  def sort
    params[:category].each_with_index do |id, index|
      Category.update_all({sort_order: index+1}, {id: id})
    end
    render nothing: true
  end

  private

    def find_category
      @category = Category.find(params[:id])
    end

end
