class Admin::AdminController < ActionController::Base

  layout 'admin'
  before_filter :authenticate_user!
  
  def index
  end

  private

    def crud_success klass=nil, verb=""
      unless klass.nil?
        redirect_to controller: "admin/#{klass.downcase.pluralize}", action: "index"
        flash[:notice] = "#{klass.titleize} #{verb}!"
      end
    end

    def crud_failure klass=nil, action="", verb="", obj=nil
      unless klass.nil?
        if obj.nil?
          redirect_to controller: "admin/#{klass.downcase.pluralize}", action: action
        else
          redirect_to controller: "admin/#{klass.downcase.pluralize}", action: "edit", id: obj.id
        end
        flash[:alert] = "#{klass.titleize} could not be #{verb}. Please try again."
      end
    end

end
