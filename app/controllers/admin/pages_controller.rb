class Admin::PagesController < Admin::AdminController

  before_filter :find_page, only: [:show, :edit, :update, :destroy]

  def index
    @pages = Page.sorted.all
  end

  def show
    render action: 'edit'
  end

  def new
    @page = Page.new
  end

  def create
    @page = Page.new(params[:page])
    if @page.save
      crud_success "page", "saved"
    else
      crud_failure "page", "new", "saved"
    end
  end

  def edit
  end

  def update
    if @page.update_attributes(params[:page])
      crud_success "page", "updated"
    else
      crud_failure "page", "edit", "updated", @page
    end
  end

  def destroy
    if @page.destroy
      crud_success "page", "deleted"
    else
      crud_failure "page", "index", "deleted"
    end
  end

  def sort
    params[:page].each_with_index do |id, index|
      Page.update_all({sort_order: index+1}, {id: id})
    end
    render nothing: true
  end

  private

    def find_page
      @page = Page.find(params[:id])
    end

end
