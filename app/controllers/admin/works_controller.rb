class Admin::WorksController < Admin::AdminController

  before_filter :find_work, only: [:show, :edit, :update, :destroy]

  def index
    @categories = Category.sorted.all
    @uncategorized = Work.uncategorized.sorted.all
    @works = Work.sorted.all
  end

  def show
    render action: 'edit'
  end

  def new
    @work = Work.new
  end

  def create
    @work = Work.new(params[:work])
    if @work.save
      crud_success "work", "saved"
    else
      crud_failure "work", "new", "saved"
    end
  end

  def edit
  end

  def update
    if @work.update_attributes(params[:work])
      crud_success "work", "updated"
    else
      crud_failure "work", "edit", "updated", @work
    end
  end

  def destroy
    if @work.destroy
      crud_success "work", "deleted"
    else
      crud_failure "work", "index", "deleted"
    end
  end

  def sort
    params[:work].each_with_index do |id, index|
      Work.update_all({sort_order: index+1}, {id: id})
    end
    render nothing: true
  end

  private

    def find_work
      @work = Work.find(params[:id])
    end

end
