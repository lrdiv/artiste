class Admin::InquiriesController < Admin::AdminController

  def index
    @inbox = Inquiry.inbox.all
    @archive = Inquiry.archive.all
  end

  def show
    @inquiry = Inquiry.find(params[:id])
  end

  def destroy
    @inquiry = Inquiry.find(params[:id])
    if @inquiry.destroy
      redirect_to admin_inquiries_path, notice: "Inquiry deleted!"
    else
      redirect_to admin_inquiries_path, alert: "Could not delete"
    end
  end

  def archive
    @inquiry = Inquiry.find(params[:id])
    @inquiry.update_attribute(:archived, true)
    respond_to :js
  end

  def unarchive
    @inquiry = Inquiry.find(params[:id])
    @inquiry.update_attribute(:archived, false)
    respond_to :js
  end

end
