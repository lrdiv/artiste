class Inquiry < ActiveRecord::Base

  attr_accessible :full_name, :email, :phone, :address, :message, :archived

  validates :full_name, presence: true
  validates :email, presence: true
  validates :message, presence: true

  scope :archive, -> { where(archived: true) }
  scope :inbox, -> { where(archived: false) }

end
