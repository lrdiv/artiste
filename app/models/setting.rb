class Setting < ActiveRecord::Base
  
  attr_accessible :title, :content

  validates :title, presence: true, uniqueness: true

end
