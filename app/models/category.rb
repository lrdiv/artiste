class Category < ActiveRecord::Base
  
  attr_accessible :title, :description, :image, :image_cache, :featured
  mount_uploader :image, ImageUploader

  has_many :works, order: "sort_order asc"

  validates :title, presence: true, uniqueness: true
  before_save :feature

  scope :sorted, -> { order("sort_order asc") }
  scope :featured, -> { where(featured: true) }

  def feature
    if self.featured == true
      Category.update_all({featured: false}, {featured: true})
    end
  end

end
