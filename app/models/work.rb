class Work < ActiveRecord::Base
  
  attr_accessible :title, :caption, :produced_at, :image, :featured, :public, :category_id, :image_cache
  mount_uploader :image, ImageUploader

  belongs_to :category

  validates :title, presence: true
  before_save :feature
  
  scope :sorted, -> { order("sort_order asc") }
  scope :featured, -> { where(featured: true) }
  scope :public, -> { where(public: true) }
  scope :uncategorized, -> { where(category_id: nil) }

  def feature
    if self.featured == true
      Work.update_all({:featured => false}, {:featured => true})
    end
  end

end
