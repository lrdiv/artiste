class Page < ActiveRecord::Base
  
  attr_accessible :title, :body, :show_in_nav, :image, :image_cache
  mount_uploader :image, ImageUploader

  validates :title, presence: true, uniqueness: true

  scope :nav, -> { where(show_in_nav: true) }
  scope :sorted, -> { order("sort_order asc") }

end
