class Event < ActiveRecord::Base
  
  attr_accessible :title, :description, :image, :event_at, :image_cache
  mount_uploader :image, ImageUploader

  validates :title, presence: true

  scope :upcoming, -> { where('event_at >= ?', Time.now.to_s(:db)) }
  scope :previous, -> { where('event_at < ?', Time.now.to_s(:db)) }
  scope :sorted, -> { order("sort_order asc") }

end
